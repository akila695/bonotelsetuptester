package com.rezgateway.automation.enums;
/**
 * @author Akila
 *
 */


public enum ContactType {

	Rate_amendments,Reservations,Product,FITWHOLESALEDESK,Ticket_Office,Wedding_Chapel,Other,SALES,Statements,Acct_AR,Acct_Credit,Activity_Reservations,Blackouts,Main_Number,Contracting,Allotments,Invoicing,NONE;

	
	//TODO: Add other types
	public static ContactType getSearchByType(String type) {

		if (type.trim().equalsIgnoreCase("FITWHOLESALE")) {
			return ContactType.FITWHOLESALEDESK;
		} else if (type.trim().equalsIgnoreCase("SALE")) {
			return ContactType.SALES;
		} 
		else if (type.trim().equalsIgnoreCase("AcctA/R")) {
			return ContactType.Acct_AR;
		}
		else if (type.trim().equalsIgnoreCase("AcctCredit")) {
			return ContactType.Acct_Credit;
		}
		else if (type.trim().equalsIgnoreCase("ActivityReservations")) {
			return ContactType.Activity_Reservations;
		}
		else if (type.trim().equalsIgnoreCase("Blackouts")) {
			return ContactType.Blackouts;
		}
		else if (type.trim().equalsIgnoreCase("Contracting")) {
			return ContactType.Contracting;
		}
		else if (type.trim().equalsIgnoreCase("Allotments")) {
			return ContactType.Allotments;
		}
		else if (type.trim().equalsIgnoreCase("Invoicing")) {
			return ContactType.Invoicing;
		}
		else if (type.trim().equalsIgnoreCase("Main Number")) {
			return ContactType.Main_Number;
		}
		else if (type.trim().equalsIgnoreCase("Product")) {
			return ContactType.Product;
		}
		else if (type.trim().equalsIgnoreCase("Reservations")) {
			return ContactType.Reservations;
		}
		else if (type.trim().equalsIgnoreCase("Rate amendments")) {
			return ContactType.Rate_amendments;
		}
		else if (type.trim().equalsIgnoreCase("Statements")) {
			return ContactType.Statements;
		}
		else if (type.trim().equalsIgnoreCase("Ticket Office")) {
			return ContactType.Ticket_Office;
		}
		else if (type.trim().equalsIgnoreCase("Wedding Chapel")) {
			return ContactType.Wedding_Chapel;
		}
		else if (type.trim().equalsIgnoreCase("Other")) {
			return ContactType.Other;
		}
		else {
			return ContactType.NONE;
		}

	}

}















