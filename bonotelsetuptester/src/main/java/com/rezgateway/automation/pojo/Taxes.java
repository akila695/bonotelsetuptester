package com.rezgateway.automation.pojo;

public class Taxes {
	
		private String From="";
		private String To="";
		private String Sales="";
		private String Energy="";
		private String MiscellaneousFees="";
		public String getFrom() {
			return From;
		}
		public void setFrom(String from) {
			From = from;
		}
		public String getTo() {
			return To;
		}
		public void setTo(String to) {
			To = to;
		}
		public String getSales() {
			return Sales;
		}
		public void setSales(String sales) {
			Sales = sales;
		}
		public String getEnergy() {
			return Energy;
		}
		public void setEnergy(String energy) {
			Energy = energy;
		}
		public String getMiscellaneousFees() {
			return MiscellaneousFees;
		}
		public void setMiscellaneousFees(String miscellaneousFees) {
			MiscellaneousFees = miscellaneousFees;
		}
		 
}
