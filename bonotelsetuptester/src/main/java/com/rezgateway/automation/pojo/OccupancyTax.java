package com.rezgateway.automation.pojo;

public class OccupancyTax {
	
		private String From="";
		private String To="";
		private String RoomType="";
		private String RatePlan="";
		public String getFrom() {
			return From;
		}
		public void setFrom(String from) {
			From = from;
		}
		public String getTo() {
			return To;
		}
		public void setTo(String to) {
			To = to;
		}
		public String getRoomType() {
			return RoomType;
		}
		public void setRoomType(String roomType) {
			RoomType = roomType;
		}
		public String getRatePlan() {
			return RatePlan;
		}
		public void setRatePlan(String ratePlan) {
			RatePlan = ratePlan;
		}
		public String getOccupancyTax() {
			return OccupancyTax;
		}
		public void setOccupancyTax(String occupancyTax) {
			OccupancyTax = occupancyTax;
		}
		private String OccupancyTax="";
}
