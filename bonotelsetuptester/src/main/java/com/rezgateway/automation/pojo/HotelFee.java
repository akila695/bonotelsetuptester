package com.rezgateway.automation.pojo;

public class HotelFee {
	
		private String From	="";
		private String To="";
		private String RoomType="";
		private String RatePlan="";
		private String FeeType="";
		private String 	FeeMethod="";
		private String  IncludeSalesTax	="";
		private String FeeAssign="";
		private String FeeRequired="";
		private String FeeFrequency="";
		public String getFrom() {
			return From;
		}
		public void setFrom(String from) {
			From = from;
		}
		public String getTo() {
			return To;
		}
		public void setTo(String to) {
			To = to;
		}
		public String getRoomType() {
			return RoomType;
		}
		public void setRoomType(String roomType) {
			RoomType = roomType;
		}
		public String getRatePlan() {
			return RatePlan;
		}
		public void setRatePlan(String ratePlan) {
			RatePlan = ratePlan;
		}
		public String getFeeType() {
			return FeeType;
		}
		public void setFeeType(String feeType) {
			FeeType = feeType;
		}
		public String getFeeMethod() {
			return FeeMethod;
		}
		public void setFeeMethod(String feeMethod) {
			FeeMethod = feeMethod;
		}
		public String getIncludeSalesTax() {
			return IncludeSalesTax;
		}
		public void setIncludeSalesTax(String includeSalesTax) {
			IncludeSalesTax = includeSalesTax;
		}
		public String getFeeAssign() {
			return FeeAssign;
		}
		public void setFeeAssign(String feeAssign) {
			FeeAssign = feeAssign;
		}
		public String getFeeRequired() {
			return FeeRequired;
		}
		public void setFeeRequired(String feeRequired) {
			FeeRequired = feeRequired;
		}
		public String getFeeFrequency() {
			return FeeFrequency;
		}
		public void setFeeFrequency(String feeFrequency) {
			FeeFrequency = feeFrequency;
		}
		public String getFeeBasedOn() {
			return FeeBasedOn;
		}
		public void setFeeBasedOn(String feeBasedOn) {
			FeeBasedOn = feeBasedOn;
		}
		public String getValue() {
			return Value;
		}
		public void setValue(String value) {
			Value = value;
		}
		public String getCondition() {
			return Condition;
		}
		public void setCondition(String condition) {
			Condition = condition;
		}
		private String FeeBasedOn="";
		private String Value="";
		private String Condition="";
		
		
				
}
